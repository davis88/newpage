(function () {
    "use strict";
    angular.module("pageApp")
        .controller("pageAppCtrl", pageAppCtrl)
        .controller("RegistrationCtrl", RegistrationCtrl);
    
    pageAppCtrl.$inject = ['PageServiceAPI',"$log"]; 
    RegistrationCtrl.$inject = ['PageServiceAPI',"$log"];

    function RegistrationCtrl(PageServiceAPI,  $log){
        var self = this;
        console.log(">>> " + PageServiceAPI.getFullname());
        self.fullname = PageServiceAPI.getFullname();
    }

    function pageAppCtrl(PageServiceAPI,  $log) {

        var self  = this;
        
        self.onSubmit = onSubmit;
        self.initForm = initForm;
        self.onReset = onReset;
        self.isLegalAge = isLegalAge;
        
        
        self.isSubmitted = false;

        self.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        self.contactFormat = /^[0-9\-\(\)\+]+$/
        
        self.user = {}

        self.countries = [
            { name: "ASEAN FOUNDING MEMBERS" , value:0},
            { name: "Singapore", value: 1},
            { name: "Malaysia", value: 2},
            { name: "Thailand", value: 3},
            { name: "Indonesia", value: 4},
            { name: "Philippine", value: 5}    
        ];

        function initForm(){
            self.user.selectedCountry = "0";
            self.isSubmitted = false;
        }

        function onReset(){
            self.user = Object.assign({}, self.user);
            self.registrationform.$setPristine();
            self.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log(PageServiceAPI);
            self.isSubmitted = true
            PageServiceAPI.register(self.user)
                .then((result)=>{
                    self.user = result;
                    console.log($log);
                    $log.info(result);
                    self.isSubmitted = true
                }).catch((error)=>{
                    console.log(error);
                })
        }
        
        function isLegalAge(day, month, year) {
                var DOB = self.user.dob

                var today = new Date();
                var birthDate = new Date(DOB);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }    
                console.log(age > 18)
                return (age > 18);
        }

        self.initForm();
    }
    
})();    